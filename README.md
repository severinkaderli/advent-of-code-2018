# Advent of Code 2018
My solutions of [Advent of Code 2018](https://adventofcode.com/2018) written in Python.

## Usage
1. Install the needed dependencies using `pip install -r requirements.txt`.
2. Run `main.py` of the specific days.
