#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 7 of Advent of Code 2018.
# https://adventofcode.com/2018/day/7
#
# USAGE:
# ./main.py
from collections import OrderedDict
import os
import re
import sys


class Task:
    step = None
    step_duration = 0
    work_time = 0
    is_done = False

    def __init__(self, step, step_duration):
        """Creates a new task object."""
        self.step = step
        self.step_duration = step_duration

    def work(self):
        """Works on the task and sets is_done accordingly."""
        self.work_time += 1

        if self.work_time == ord(self.step) - 64 + self.step_duration:
            self.is_done = True


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return [parse_line(line) for line in file.read().splitlines()]


def parse_line(line):
    """Parses a line for the steps."""
    pattern = re.compile(r"Step\s(\w).+step\s(\w).+")
    matches = pattern.match(line)
    return (matches.group(1), matches.group(2))


def build_graph():
    """Builds a graph using the given edges."""
    edges = get_input()
    graph = OrderedDict()
    for edge in edges:
        if edge[0] not in graph:
            graph[edge[0]] = set()

        if edge[1] not in graph:
            graph[edge[1]] = set()

        graph[edge[1]].add(edge[0])

    return graph


def order_graph(graph):
    """Orders a graph based on the number of parents a node has."""
    return OrderedDict(sorted(graph.items(), key=lambda x: (len(x[1]), x)))


def get_order(graph):
    """Gets the topological ordering of the given graph."""
    order = []
    while len(graph) > 0:
        graph = order_graph(graph)

        first_key = list(graph.keys())[0]
        for __, edges in graph.items():
            if first_key in edges:
                edges.remove(first_key)

        order.append(first_key)
        graph.pop(first_key, None)

    return order


def part_one():
    """In what order should the steps in your instructions be completed?"""
    return "".join(get_order(build_graph()))


def get_work_duration(number_of_workers, step_duration):
    graph = build_graph()

    # The total work duration
    work_duration = 0

    # Dictionary of workers with the assigned task
    workers = {i: None for i in range(number_of_workers)}

    # Whether all tasks are finished or not
    done = False

    while not done:
        done = True
        work_duration += 1

        # Check if a worker is done with a task
        for worker, task in workers.items():
            if task:
                task.work()

                if task.is_done:
                    # If is done remove the task from the graph
                    for __, edges in graph.items():
                        if task.step in edges:
                            edges.remove(task.step)

                    workers[worker] = None
                else:
                    done = False

        # Distribute free tasks to free workers
        for worker, task in workers.items():
            if not task and len(graph) > 0:
                graph = order_graph(graph)
                first_key = list(graph.keys())[0]
                if len(graph[first_key]) == 0:
                    workers[worker] = Task(first_key, step_duration)
                    graph.pop(first_key, None)
                    done = False

    return work_duration - 1


def part_two():
    """With 5 workers and the 60+ second step durations described above, how
    long will it take to complete all of the steps?"""
    return get_work_duration(5, 60)


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
