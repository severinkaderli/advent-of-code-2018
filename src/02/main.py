#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 2 of Advent of Code 2018.
# https://adventofcode.com/2018/day/2
#
# USAGE:
# ./main.py
import itertools
import os
import sys


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return file.read().splitlines()


def part_one():
    """What is the checksum for your list of box IDs?"""
    ids = get_input()
    number_of_two_occurences = 0
    number_of_three_occurences = 0

    for id in ids:
        if True in set(id.count(char) == 2 for char in id):
            number_of_two_occurences += 1

        if True in set(id.count(char) == 3 for char in id):
            number_of_three_occurences += 1

    return number_of_two_occurences * number_of_three_occurences


def part_two():
    """What letters are common between the two correct box IDs?"""
    ids = get_input()
    for id_x, id_y in itertools.product(ids, repeat=2):
        number_of_diffs = 0
        last_diff_index = 0

        for index in range(len(id_x)):
            if id_x[index] != id_y[index]:
                number_of_diffs += 1
                last_diff_index = index

            if index == len(id_x) - 1 and number_of_diffs == 1:
                return id_x[:last_diff_index] + id_x[last_diff_index + 1:]


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
