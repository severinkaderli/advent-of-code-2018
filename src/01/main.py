#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 1 of Advent of Code 2018.
# https://adventofcode.com/2018/day/1
#
# USAGE:
# ./main.py
import os
import sys


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return list(map(int, file.read().splitlines()))


def part_one():
    """Starting with a frequency of zero, what is the resulting frequency after
    all of the changes in frequency have been applied?"""
    return sum(get_input())


def part_two():
    """What is the first frequency your device reaches twice?"""
    frequency = 0
    frequency_history = set()
    changes = get_input()
    while True:
        for delta in changes:
            if frequency in frequency_history:
                return frequency

            frequency_history.add(frequency)
            frequency += delta


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
