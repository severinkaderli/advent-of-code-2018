#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 5 of Advent of Code 2018.
# https://adventofcode.com/2018/day/5
#
# USAGE:
# ./main.py
import os
import string
import sys


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return file.read().replace("\n", "")


def compare_chars(a, b):
    """Checks if a and b are a sequence of the same character in lower and
    uppercase or vice versa."""
    return ord(a) + 32 == ord(b) or ord(a) - 32 == ord(b)


def react(polymer):
    """React the polymer and return it."""
    i = 0
    while(i < len(polymer) - 1):
        if compare_chars(polymer[i], polymer[i + 1]):
            polymer = polymer[:i] + polymer[i + 2:]
            if i > 0:
                i -= 1
        else:
            i += 1

    return polymer


def part_one():
    """How many units remain after fully reacting the polymer you scanned?"""
    return len(react(get_input()))


def part_two():
    """What is the length of the shortest polymer you can produce by removing
    all units of exactly one type and fully reacting the result?"""
    polymer = react(get_input())
    shortest_size = sys.maxsize

    for c in string.ascii_lowercase:
        new_polymer = react(polymer.replace(c, "").replace(c.upper(), ""))
        if len(new_polymer) < shortest_size:
            shortest_size = len(new_polymer)

    return shortest_size


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
