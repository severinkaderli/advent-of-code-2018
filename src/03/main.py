#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 3 of Advent of Code 2018.
# https://adventofcode.com/2018/day/3
#
# USAGE:
# ./main.py
import itertools
import os
import re
import sys

"""The dimensions of the fabric."""
FABRIC_SIZE = 1000


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return list(map(parse_claim, file.read().splitlines()))


def parse_claim(claim):
    """Parses a claim to a list."""
    pattern = re.compile(r"#(\w+)\s@\s(\w+),(\w+):\s(\w+)x(\w+)")
    matches = pattern.match(claim)
    return list(map(int, matches.groups()))


def build_fabric():
    """Build the fabric matrix and returns it."""
    claims = get_input()
    fabric = [[0 for x in range(FABRIC_SIZE)] for y in range(FABRIC_SIZE)]
    for claim in claims:
        for x in range(claim[1], claim[1] + claim[3]):
            for y in range(claim[2], claim[2] + claim[4]):
                fabric[x][y] += 1

    return fabric


def part_one():
    """How many square inches of fabric are within two or more claims?"""
    fabric = build_fabric()

    sum = 0
    for x, y in itertools.product(range(FABRIC_SIZE), repeat=2):
        if fabric[x][y] > 1:
            sum += 1

    return sum


def part_two():
    """What is the ID of the only claim that doesn't overlap?"""
    claims = get_input()
    fabric = build_fabric()

    for claim in claims:
        is_occupied = False
        for x in range(claim[1], claim[1] + claim[3]):
            for y in range(claim[2], claim[2] + claim[4]):
                if fabric[x][y] > 1:
                    is_occupied = True

        if not is_occupied:
            return claim[0]


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
