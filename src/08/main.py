#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 8 of Advent of Code 2018.
# https://adventofcode.com/2018/day/8
#
# USAGE:
# ./main.py
import os
import sys


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        return [int(value) for value in file.read().split()]


def part_one():
    """"""
    input = get_input()

    metadata = 0
    current_level = 0
    children_left = 0
    number_of_metadata_field = []

    i = 0
    while i < len(input) - 2:
        value = input[i]
        print(i, ":", value)
        # First entry
        if value > 0 and children_left == 0 and len(number_of_metadata_field) > 0:
            number_of_metadata = number_of_metadata_field.pop()
            for __ in range(number_of_metadata):
                metadata += input[i + 1]
                i += 1

        elif value > 0:
            if children_left > 0:
                children_left -= 1
                number_of_metadata = number_of_metadata_field.pop()
                print("numbers:", number_of_metadata)
                i += 1

                for __ in range(number_of_metadata):
                    metadata += input[i + 1]
                    i += 1

            children_left += value
            number_of_metadata_field.append(input[i + 1])
            i += 1
        # Child
        else:
            children_left -= 1
            number_of_metadata = input[i + 1]
            print("numbers:", number_of_metadata)
            i += 1

            for __ in range(number_of_metadata):
                metadata += input[i + 1]
                i += 1

        i += 1

    print(metadata)
    return True


def part_two():
    """"""
    return True


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
