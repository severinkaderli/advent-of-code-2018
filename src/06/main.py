#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 6 of Advent of Code 2018.
# https://adventofcode.com/2018/day/6
#
# USAGE:
# ./main.py
import os
import sys


class Point:
    x = 0
    y = 0

    def __init__(self, x, y):
        """Creates a new point using with x and y coordinates."""
        self.x = x
        self.y = y

    def distance(self, other):
        """Get the manhatten distance between two points."""
        return abs(self.x - other.x) + abs(self.y - other.y)

    def __str__(self):
        """String representation of the object."""
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def __repr__(self):
        """Representation of the object."""
        return self.__str__()


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        lines = file.read().splitlines()
        coordinates = []
        for line in lines:
            x, y = line.split(",")
            coordinates.append(Point(int(x), int(y)))
        return coordinates


def get_closest_point(location, coordinates):
    """Gets the closest point from the location."""
    selected_point = None
    smallest_distance = sys.maxsize
    number_of_points = None

    for point in coordinates:
        distance = location.distance(point)

        if distance == smallest_distance:
            number_of_points += 1

        if distance < smallest_distance:
            number_of_points = 1
            smallest_distance = distance
            selected_point = point

    # If multiple points share the closest distance return -1
    if number_of_points > 1:
        return -1
    else:
        return coordinates.index(selected_point)


def build_map():
    """Builds the map using the coordinates."""
    points = get_input()
    max_x = max(point.x for point in points)
    max_y = max(point.y for point in points)

    map = [[None for x in range(max_x + 1)] for y in range(max_y + 1)]
    for x in range(max_x + 1):
        for y in range(max_y + 1):
            map[y][x] = get_closest_point(Point(x, y), points)

    return map


def part_one():
    """What is the size of the largest area that isn't infinite?"""
    map = build_map()
    infinite_areas = set([-1])
    areas = {}
    max_y = len(map)
    max_x = len(map[0])

    for y in range(max_y):
        for x in range(max_x):
            area = map[y][x]

            if area in infinite_areas:
                continue

            if x == 0 or y == 0 or x == max_x - 1 or y == max_y - 1:
                areas.pop(area, None)
                infinite_areas.add(area)

            if area not in areas:
                areas[area] = 1
            else:
                areas[area] += 1

    return areas[max(areas, key=areas.get)]


def is_within_distance_to_points(check_point, points, target_distance):
    """Checks if the point is within the target distance to all coordinates."""
    total_distance = 0

    for point in points:
        total_distance += check_point.distance(point)

        if total_distance >= target_distance:
            return False

    return total_distance < target_distance


def part_two():
    """What is the size of the region containing all locations which have a
    total distance to all given coordinates of less than 10000?"""
    points = get_input()
    map = build_map()
    number_of_points = 0

    for x in range(len(map)):
        for y in range(len(map[0])):
            if is_within_distance_to_points(Point(x, y), points, 10000):
                number_of_points += 1

    return number_of_points


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
