#!/usr/bin/env python3
#
# SCRIPT NAME:
# main.py
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# Solution for day 4 of Advent of Code 2018.
# https://adventofcode.com/2018/day/4
#
# USAGE:
# ./main.py
import os
import re
import sys


def get_input():
    """Prepare the puzzle input for use."""
    script_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/"
    with open(script_path + "input.txt", "r") as file:
        entries = file.read().splitlines()
        entries.sort()
        return entries


def get_minute(entry):
    """Return the minute of the entry."""
    pattern = re.compile(r".+:(\d{2}).+")
    matches = pattern.match(entry)
    return int(matches.group(1))


def get_guard(entry):
    """Returns the guard number of the entry."""
    pattern = re.compile(r".+#(\d+).+")
    matches = pattern.match(entry)
    return int(matches.group(1))


def log_time(log, guard, start, end):
    """Logs the time for the guard."""
    for i in range(int(start), int(end)):
        log[guard][i] += 1


def build_log():
    """Builds the sleep log for the guards."""
    log = {}
    active_guard = 0
    previous_time = 0

    for entry in get_input():
        # Begins shift
        if "begins" in entry:
            active_guard = get_guard(entry)
            if active_guard not in log:
                log[active_guard] = dict.fromkeys(range(0, 60), 0)
        # Falls asleep
        elif "falls" in entry:
            previous_time = get_minute(entry)
        # Awakes
        else:
            log_time(log, active_guard, previous_time, get_minute(entry))

    return log


def part_one():
    """Find the guard that has the most minutes asleep. What minute does that
    guard spend asleep the most?"""
    log = build_log()

    selected_guard = 0
    most_minutes_asleep = 0
    for guard, sleep_times in log.items():
        minutes_asleep = sum(sleep_times.values())

        if minutes_asleep > most_minutes_asleep:
            most_minutes_asleep = minutes_asleep
            selected_guard = guard

    sleepiest_minute = max(log[selected_guard], key=log[selected_guard].get)
    return selected_guard * sleepiest_minute


def part_two():
    """Of all guards, which guard is most frequently asleep on the same
    minute?"""
    log = build_log()

    selected_guard = 0
    selected_minute = 0
    highest_sleep_amount = 0
    for guard, sleep_times in log.items():
        for minute, sleep_amount in sleep_times.items():
            if sleep_amount > highest_sleep_amount:
                highest_sleep_amount = sleep_amount
                selected_minute = minute
                selected_guard = guard

    return selected_guard * selected_minute


if __name__ == "__main__":
    print("Solution Part 1:", part_one())
    print("Solution Part 2:", part_two())
